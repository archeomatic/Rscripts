##@rcheomatic=group
##showplots
# Affiche Layer et demande en entree sera une couche vecteur
##Layer=vector
##Orientation=Field Layer
##Titre_du_graphique=string histogramme circulaire \nd'orientation des sepultures
##Epaisseur_des_segments=number 2
##Couleur_des_segments_en_anglais=string orange
##Graduations_en_degres=number 10

# Appel du package "plotrix"..verifier qu'il est bien installe dans R
library("plotrix")
# Declaration de la variable orient = champ Orientation
orient <- Layer[[Orientation]]
# Declaration de la variable recap = recapitulation du champ Orientation
# cad nombre d occurences pour chaque valeur unique d'orientation
recap <- aggregate(orient,by=list(orient),length)
# creation du diagramme
diag <- polar.plot(
# longueur des segments = nombres de sepultures ayant la meme orientation
c(0,recap$x),
# angle des segments
c(0,recap$Group.1),
# Titre
main=Titre_du_graphique,
# Origine (placement du 0) si 0 il sera a droite, si 90 en haut, si 180 a gauche
start=90,
# Sens horaire
clockwise=TRUE,
# Epaisseur des segments
lwd=Epaisseur_des_segments,
# Couleur des segments
line.col=Couleur_des_segments_en_anglais,
# Emplacement des etiquettes sur les cercles concentriques (nombre de sepultures)
# si 0 y en a pas, si 1 en bas, si 2 a gauche, etc
show.grid.label=3,
# Affichage des cercles concentriques (TRUE/FALSE)
show.radial.grid=TRUE,
# Etiquettes a lexterieur du diagramme
# par defaut (on efface le parametre) en degre de 20 en 20
# ex= seq(0,350,by=10) veut dire de 0 a 350 avec un pas de 10
# ex= pour les points cardinaux on peux ecrire labels=c
labels=seq(0,360-Graduations_en_degres,by=Graduations_en_degres),
# Emplacement des etiquettes a l'exterieur du diagramme (a faire concorder avec labels)
# (ex= de 0 a 315 avec un pas de 45 = (0,315,by=45)
# si on a choisi les points cardinaux comme dans le commentaire precedent
label.pos=seq(0,360-Graduations_en_degres,by=Graduations_en_degres)*2*pi/360)
# Derniere remarque ne pas mettre un seul accent !! meme dans les commentaires !!

