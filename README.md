# Rscripts

Dépots de scripts R -généralement- utiles ou du moins utilisable en archéologie !!

Vous pouvez:
- soit télecharger tous le dépot (cette page et les dossiers contenant les différents scripts) en cliquant sur [code] → télecharger en .zip
- soit les fichiers un à un

Note: ces dépôts ont pu être déposé pour plusieurs raisons:
- pour rendre des cripts accessibles depuis le site archeomatic.wordpress.com
- pour garder en archive des scripts utilisés dans différents projets de recherche et/ou de publication
- parce que j'en avais envie !